package hepta.crud.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class LotacaoDTO{
	private Integer id;
	private String nome;
	private String ramal;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getRamal() {
		return ramal;
	}
	public void setRamal(String ramal) {
		this.ramal = ramal;
	}

}

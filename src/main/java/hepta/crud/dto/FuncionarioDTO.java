package hepta.crud.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import hepta.crud.entity.Lotacao;

@XmlRootElement
public class FuncionarioDTO{

	private Integer id;
	private String nome;
	private String telefone;
	private Integer idLotacao;
	
	public Integer getIdLotacao() {
		return idLotacao;
	}
	
	public void setLotacao(Integer idLotacao) {
		this.idLotacao = idLotacao;
	}
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

}

package hepta.crud.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.SQLError;

import hepta.crud.entity.Funcionario;

public class FuncionarioDAO {
	public String insertFuncionario(Funcionario func) {
		String sql = null;
		if (func.getidLotacao() == null) {
			sql = String.format("INSERT INTO funcionarios(nome, telefone) VALUES(\"%s\",\"%s\")", func.getNome(), func.getTelefone());
		}else {
			sql = String.format("INSERT INTO funcionarios(nome, telefone, id_lotacao) VALUES(\"%s\",\"%s\",%d)", func.getNome(), func.getTelefone(), func.getidLotacao());
		}
		ConnectionFactory conFac = new ConnectionFactory();
		String retorno = null;
		try{
			retorno = conFac.execute(sql, conFac.createConnection());
			
		}catch (Exception e) {
			return "Não foi possível fechar a conexão com o banco";
		}
		
		
		return retorno;
	}
	
	public String atualizarFuncionario(Funcionario func) {
		String sql = null;
		sql = String.format("UPDATE gestao.funcionarios SET nome = \"%s\", telefone = \"%s\", id_lotacao = %d WHERE id = %d;", func.getNome(), func.getTelefone(), func.getidLotacao(), func.getId());

		ConnectionFactory conFac = new ConnectionFactory();
		String retorno = null;
		try{
			retorno = conFac.execute(sql, conFac.createConnection());
		}catch (Exception e) {
			return "Não foi possível fechar a conexão com o banco";
		}
		
		return retorno;
	}

	public List<Funcionario> listar(){
		List<Funcionario> lista = new ArrayList<Funcionario>();
		try {
			String sql = "SELECT funcionarios.id, funcionarios.nome, funcionarios.telefone, funcionarios.id_lotacao FROM gestao.funcionarios;";

			ConnectionFactory conFac = new ConnectionFactory();
			Connection conexao = conFac.createConnection(); 
			ResultSet resultado = conFac.select(sql, conexao);
			
			while(resultado.next()) {
				Funcionario func = new Funcionario();
				func.setId(resultado.getInt("id"));
				func.setNome(resultado.getString("nome"));
				func.setTelefone(resultado.getString("telefone"));
				
				func.setidLotacao(resultado.getInt("id_lotacao"));
				lista.add(func);
			}

			conexao.close();
			
		}catch (Exception e) {
			System.out.println("Não foi possível executar o select de todos os funcionários\n\n");
			System.out.println(e);
			return lista;
		}
		return lista;
	}
	public List<Funcionario> listarPorLotacao(Integer id){
		List<Funcionario> lista = new ArrayList<Funcionario>();
		try {
			String sql = String.format("SELECT funcionarios.id, funcionarios.nome, funcionarios.telefone, funcionarios.id_lotacao FROM gestao.funcionarios WHERE id_lotacao = %d", id);

			ConnectionFactory conFac = new ConnectionFactory();
			Connection conexao = conFac.createConnection(); 
			ResultSet resultado = conFac.select(sql, conexao);
			
			while(resultado.next()) {
				Funcionario func = new Funcionario();
				func.setId(resultado.getInt("id"));
				func.setNome(resultado.getString("nome"));
				func.setTelefone(resultado.getString("telefone"));
				
				func.setidLotacao(resultado.getInt("id_lotacao"));
				lista.add(func);
			}

			conexao.close();
			
		}catch (Exception e) {
			System.out.println("Não foi possível executar o select de todos os funcionários\n\n");
			System.out.println(e);
			return lista;
		}
		return lista;
	}
	public Funcionario selectOne(Integer id){
		String sql = String.format("SELECT funcionarios.id, funcionarios.nome, funcionarios.telefone, funcionarios.id_lotacao FROM gestao.funcionarios WHERE id=%d;", id);
		Funcionario func = new Funcionario();
		try {

			ConnectionFactory conFac = new ConnectionFactory();
			Connection conexao = conFac.createConnection(); 
			ResultSet resultado = conFac.select(sql, conexao);

			while(resultado.next()) {
				func.setId(resultado.getInt("id"));
				func.setNome(resultado.getString("nome"));
				func.setTelefone(resultado.getString("telefone"));
				
				func.setidLotacao(resultado.getInt("id_lotacao"));
			}
			conexao.close();
			
		}catch (Exception e) {
			System.out.println("Não foi possível executar o select do funcionario\n\n");
			System.out.println(e);
			return func;
		}
		return func;
	}
	
	public String deletarFuncionario(Integer id) {
		String sql = String.format("DELETE FROM gestao.funcionarios WHERE id=%d;", id);
		try {
			ConnectionFactory conFac = new ConnectionFactory();
			String retorno = conFac.execute(sql, conFac.createConnection());
			return null;
		} catch (Exception e) {
			return String.format("Problema ao excuir o funcionário com id %d\nO problema identificado no banco é %s", id, e.toString()); 
		}
	}
	

}

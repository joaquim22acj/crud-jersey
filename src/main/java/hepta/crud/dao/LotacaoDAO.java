package hepta.crud.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import hepta.crud.entity.Funcionario;
import hepta.crud.entity.Lotacao;

public class LotacaoDAO {
	public List<Lotacao> listar(){
		List<Lotacao> lista = new ArrayList<Lotacao>();
		String sql = "SELECT lotacao.id, lotacao.nome, lotacao.ramal FROM gestao.lotacao;";
		try {
			ConnectionFactory conFac = new ConnectionFactory();
			Connection conexao = conFac.createConnection(); 
			ResultSet resultado = conFac.select(sql, conexao);
			
			while(resultado.next()) {
				Lotacao lotacao = new Lotacao();
				lotacao.setId(resultado.getInt("id"));
				lotacao.setNome(resultado.getString("nome"));
				lotacao.setRamal(resultado.getString("ramal"));
				lista.add(lotacao);
			}

			conexao.close();
			
		}catch (Exception e) {
			System.out.println("Não foi possível executar o select de todos os funcionários\n\n");
			System.out.println(e);
			return lista;
		}
		return lista;
	}
	public Lotacao selectLotacaoById(int id) {
		Lotacao lotacao = new Lotacao();
		String sql =  String.format("SELECT lotacao.id, lotacao.nome, lotacao.ramal FROM gestao.lotacao WHERE id = %d;", id);
		try {
			ConnectionFactory conFac = new ConnectionFactory();
			
			Connection conexao = conFac.createConnection(); 
			ResultSet resultado = conFac.select(sql, conexao);
			while(resultado.next()) {
				lotacao.setId(resultado.getInt("id"));
				lotacao.setNome(resultado.getString("nome"));
				lotacao.setRamal(resultado.getString("ramal"));
			}
			conexao.close();
			}catch (Exception e) {
				System.out.println("Não foi possível executar o select da lotacao\n\n");
				System.out.println(e);
				return lotacao;
			}
		return lotacao;
	}
	public String insertLotacao(Lotacao lotacao) {
		String sql = String.format("INSERT INTO gestao.lotacao(nome, ramal) VALUES(\"%s\",\"%s\")", lotacao.getNome(), lotacao.getRamal());
		ConnectionFactory conFac = new ConnectionFactory();
		String retorno = null;
		try{
			retorno = conFac.execute(sql, conFac.createConnection());
			
		}catch (Exception e) {
			return "Não foi possível fechar a conexão com o banco";
		}
		
		
		return retorno;
	}
	public String atualizarLotacao(Lotacao lotacao) {
		String sql = null;
		sql = String.format("UPDATE gestao.lotacao SET nome = \"%s\", ramal = \"%s\" WHERE id = %d;", lotacao.getNome(), lotacao.getRamal(), lotacao.getId());

		ConnectionFactory conFac = new ConnectionFactory();
		String retorno = null;
		try{
			retorno = conFac.execute(sql, conFac.createConnection());
		}catch (Exception e) {
			return "Não foi possível fechar a conexão com o banco";
		}
		
		return retorno;
	}

	public String deletarLotacao(Integer id) {
		String sql = String.format("DELETE FROM gestao.lotacao WHERE id=%d;", id);
		try {
			ConnectionFactory conFac = new ConnectionFactory();
			String retorno = conFac.execute(sql, conFac.createConnection());
			return null;
		} catch (Exception e) {
			return String.format("Problema ao excluir o lotacao com id %d\nO problema identificado no banco é %s", id, e.toString()); 
		}
	}
}

package hepta.crud.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ConnectionFactory {

	public static Connection createConnection() throws SQLException {
		String url = "jdbc:mysql://localhost:3306/gestao";
		String user = "root";
		String password = "root";

		Connection conexao = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conexao = DriverManager.getConnection(url, user, password);
		} catch (ClassNotFoundException e) {
			System.out.println("Não foi possível conectar ao banco");
			e.printStackTrace();
		}  

		return conexao;
	}

	public String execute(String sql, Connection conexao) throws SQLException {
		PreparedStatement ps = null;
		try {
			conexao.setAutoCommit(false);
			ps = conexao.prepareStatement(sql);
			ps.executeUpdate();

			// Grava as informações se caso de problema os dados não são gravados
			conexao.commit();

		} catch (SQLException e) {
			System.out.println("Erro\n\n");
			System.out.println(e);
			if (conexao != null) {
				try {
					conexao.rollback();
					return String.format("Rollback efetuado na transação %s", e.toString());
				} catch (SQLException e2) {
					return "Erro na transação!";
				}
			}
		} finally {
			if (ps != null) {
				ps.close();
			}
			conexao.setAutoCommit(true);
			conexao.close();
		}
		return "ok";
	}
	public ResultSet select(String sql, Connection conexao) throws SQLException{
		ResultSet rs = null;
		try {
			rs = conexao.createStatement().executeQuery(sql);
		}catch (Exception e) {
			System.out.println("Não foi possível executar select");
			return null;
		}
		return rs;
	}
}
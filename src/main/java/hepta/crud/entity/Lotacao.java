package hepta.crud.entity;

import java.util.regex.Pattern;

public class Lotacao {
	private Integer id;
	private String nome;
	private String ramal;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public boolean setNome(String nome) {
		if (nome.length()>=45) {
			return false;
		}
		this.nome = nome;
		return true;
	}
	public String getRamal() {
		return ramal;
	}
	public boolean setRamal(String ramal) {
		if (ramal.length()>=45) {
			return false;
		}
		if (!Pattern.compile("\\d{4}\\-\\d{4}").matcher(ramal).matches()) {
			return false;
		}
		this.ramal = ramal;
		return true;
	}
	
}

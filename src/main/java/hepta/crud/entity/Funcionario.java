package hepta.crud.entity;

import java.util.regex.*;

public class Funcionario {

	private Integer id;
	private String nome;
	private String telefone;
	private Integer idLotacao;

	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public boolean setNome(String nome) {
		if (nome.length()>=200) {
			return false;
		}
		this.nome = nome;
		return true;
	}

	public String getTelefone() {
		return telefone;
	}

	public boolean setTelefone(String telefone) {
		if (telefone.length()>=45) {
			return false;
		}
		
		if (!Pattern.compile("\\d{4}\\-\\d{4}").matcher(telefone).matches()) {
			return false;
		}
		this.telefone = telefone;
		return true;
	}

	public Integer getidLotacao() {
		return idLotacao;
	}

	public void setidLotacao(Integer idLotacao) {
		this.idLotacao = idLotacao;
	}

}

package hepta.crud.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import hepta.crud.dao.FuncionarioDAO;
import hepta.crud.dao.LotacaoDAO;
import hepta.crud.dto.FuncionarioDTO;
import hepta.crud.dto.LotacaoDTO;
import hepta.crud.entity.Funcionario;
import hepta.crud.entity.Lotacao;

@Path("/lotacao")
public class LotacaoService {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/")
	public Response getTotalRegistros() throws Exception {
		LotacaoDAO lotacaoDAO = new LotacaoDAO();
		List<Lotacao> lista = null;
		try {
			lista = lotacaoDAO.listar();
			if (lista == null) {
				return Response.status(Status.NOT_FOUND).build();
			}
		} catch (Exception e) {
			System.out.println(e);
			return Response.status(Status.BAD_REQUEST).entity("Não foi possível executar o get").build();
		}
		return Response.status(Status.OK).entity(lista).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Response getLotacao(@PathParam("id") Integer id) throws Exception {
		LotacaoDAO lotacaoDAO = new LotacaoDAO();
		Lotacao lotacao = new Lotacao();
		try {
			lotacao = lotacaoDAO.selectLotacaoById(id);
			if (lotacao.getId() == null) {
				return Response.status(Status.NOT_FOUND).entity("Lotação não encontrada").build();
			}
		} catch (Exception e) {
			System.out.println(e);
			return Response.status(Status.BAD_REQUEST).entity("Não foi possível executar o get").build();
		}
		return Response.status(Status.OK).entity(lotacao).build();
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/")
	public Response inserirLotacaos(final LotacaoDTO dto) throws Exception {

		if (dto != null) {

			Lotacao lotacao = new Lotacao();

			/* TODO realizar a verificação do set de id de lotação */
			if (!lotacao.setNome(dto.getNome())) {
				return Response.status(Status.BAD_REQUEST).entity("Nome não está de acordo com string até 45 caracteres").build();
			}
			if (!lotacao.setRamal(dto.getRamal())) {
				return Response.status(Status.BAD_REQUEST).entity("Telefone não está de acordo com o formato de número ****-****").build();
			}
			LotacaoDAO lotacaoDAO = new LotacaoDAO();
			String resultado = lotacaoDAO.insertLotacao(lotacao);

			return Response.status(Status.CREATED).entity(resultado).build();
		} else {
			return Response.status(Status.BAD_REQUEST).entity("Não houve dado de entrada").build();
		}

	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Response atualizarLotacao(@PathParam("id") Integer id, LotacaoDTO dto) throws Exception {
		if (dto != null) {
			Lotacao lotacao = new Lotacao();
			lotacao.setId(id);
			if (!lotacao.setNome(dto.getNome())) {
				return Response.status(Status.BAD_REQUEST).entity("Nome não está de acordo com string até 200 caracteres").build();
			}
			if (!lotacao.setRamal(dto.getRamal())) {
				return Response.status(Status.BAD_REQUEST).entity("Telefone não está de acordo com string até 45 caracteres").build();
			}
				LotacaoDAO lotacaoDAO = new LotacaoDAO();
				String resultado = lotacaoDAO.atualizarLotacao(lotacao);
				return Response.status(Status.OK).entity(resultado).build();
			} else {
				return Response.status(Status.BAD_REQUEST).entity("Não houve dado de entrada").build();
			}
	}

	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Response deletarPessoar(@PathParam("id") Integer id) throws Exception {
		LotacaoDAO lotacaoDAO = new LotacaoDAO();
		String resultado = lotacaoDAO.deletarLotacao(id);
		if (resultado == null) {
			return Response.status(Status.OK).build();
		} else {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(resultado).build();
		}
	}

}

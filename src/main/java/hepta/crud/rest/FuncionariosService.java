package hepta.crud.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import hepta.crud.dao.FuncionarioDAO;
import hepta.crud.dto.FuncionarioDTO;
import hepta.crud.entity.Funcionario;

@Path("/funcionarios")
public class FuncionariosService {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/")
	public Response getTotalRegistros() throws Exception {
		FuncionarioDAO funcDAO = new FuncionarioDAO();
		List<Funcionario> lista = null;
		try {
			lista = funcDAO.listar();
			if (lista == null) {
				return Response.status(Status.NOT_FOUND).build();
			}
		}catch (Exception e) {
			System.out.println(e);
			return Response.status(Status.BAD_REQUEST).entity("Não foi possível executar o get").build();
		}
		return Response.status(Status.OK).entity(lista).build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Response getFuncionario(@PathParam("id") Integer id) throws Exception {
		FuncionarioDAO funcDAO = new FuncionarioDAO();
		Funcionario funcionario = new Funcionario(); 
		try {
			funcionario = funcDAO.selectOne(id);
			if (funcionario.getId() == null) {
				return Response.status(Status.NOT_FOUND).entity("Funcionário não encontrado").build();
			}
		}catch (Exception e) {
			System.out.println(e);
			return Response.status(Status.BAD_REQUEST).entity("Não foi possível executar o get").build();
		}
		return Response.status(Status.OK).entity(funcionario).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/lotacao/{id}")
	public Response getFuncionariosPorLotacao(@PathParam("id") Integer id) throws Exception {
		FuncionarioDAO funcDAO = new FuncionarioDAO();
		List<Funcionario> lista = null;
		try {
			lista = funcDAO.listarPorLotacao(id);
			if (lista == null) {
				return Response.status(Status.NOT_FOUND).build();
			}
		}catch (Exception e) {
			System.out.println(e);
			return Response.status(Status.BAD_REQUEST).entity("Não foi possível executar o get").build();
		}
		return Response.status(Status.OK).entity(lista).build();
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/")
	public Response inserirFuncionario(final FuncionarioDTO dto) throws Exception {

		if (dto != null) {

			Funcionario func = new Funcionario();
			func.setidLotacao(dto.getIdLotacao());
			/*TODO realizar a verificação do set de id de lotação*/
			if (!func.setNome(dto.getNome())) {
				return Response.status(Status.BAD_REQUEST).entity("Nome não está de acordo com string até 200 caracteres").build();
			}
			if (!func.setTelefone(dto.getTelefone())) {
				return Response.status(Status.BAD_REQUEST).entity("Telefone não está de acordo com o formato de número ****-****").build();
			}
			FuncionarioDAO funcDAO = new FuncionarioDAO();
			String resultado = funcDAO.insertFuncionario(func);
			
			return Response.status(Status.CREATED).entity(resultado).build();
		}else {
			return Response.status(Status.BAD_REQUEST).entity("Não houve dado de entrada").build();
		}

	}
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Response atualizarFuncionario(@PathParam("id") Integer id, FuncionarioDTO dto) throws Exception{
		if (dto != null) {
			Funcionario func = new Funcionario();
			func.setId(id);
			func.setidLotacao(dto.getIdLotacao());
			if (!func.setNome(dto.getNome())) {
				return Response.status(Status.BAD_REQUEST).entity("Nome não está de acordo com string até 200 caracteres").build();
			}
			if (!func.setTelefone(dto.getTelefone())) {
				return Response.status(Status.BAD_REQUEST).entity("Telefone não está de acordo com string até 45 caracteres").build();
			}
			FuncionarioDAO funcDAO = new FuncionarioDAO();
			String resultado = funcDAO.atualizarFuncionario(func);
			return Response.status(Status.OK).entity(resultado).build();
		}else {
			return Response.status(Status.BAD_REQUEST).entity("Não houve dado de entrada").build();
		}
	}
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Response deletarFuncionario(@PathParam("id") Integer id) throws Exception{
		FuncionarioDAO funcDAO = new FuncionarioDAO();
		String resultado = funcDAO.deletarFuncionario(id);
		if (resultado == null) {
			return Response.status(Status.OK).entity("ok").build();
		}else{
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(resultado).build();
		}
	}
	
	
}

package hepta.crud.rest;

import static org.junit.Assert.assertNotNull;

import java.net.URI;
import java.util.List;
import java.util.Random;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;
import org.junit.Before;
import org.junit.Test;

import hepta.crud.dto.FuncionarioDTO;

public class FuncionariosRESTTest {

	WebTarget service;

	@Before
	public void setUp() throws Exception {
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		service = client.target(getBaseURI());
	}

	private static URI getBaseURI() {
		return UriBuilder.fromUri("http://localhost:8080/crud-jersey").build();
	}

	@Test
	public void testPostFuncionario() {
		FuncionarioDTO dto = new FuncionarioDTO();
		Random rand = new Random();
		dto.setNome(String.format("Teste de post %d", rand.nextInt(99)));
		dto.setTelefone(String.format("%d%d%d%d-%d%d%d%d", rand.nextInt(9), rand.nextInt(9), rand.nextInt(9), rand.nextInt(9), rand.nextInt(9), rand.nextInt(9), rand.nextInt(9), rand.nextInt(9)));
		dto.setLotacao(1);
		Response response = service.path("api").path("funcionarios").request().post(Entity.entity(dto, MediaType.APPLICATION_JSON), Response.class);

		assert response.getStatus() == 201;
	}
	@Test
	public void testPostFuncionario400() {
		FuncionarioDTO dto = new FuncionarioDTO();
		Random rand = new Random();
		dto.setNome("asdfasdfffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff");
		dto.setTelefone(String.format("%d%d%d%d-%d%d%d%d", rand.nextInt(9), rand.nextInt(9), rand.nextInt(9), rand.nextInt(9), rand.nextInt(9), rand.nextInt(9), rand.nextInt(9), rand.nextInt(9)));
		dto.setLotacao(1);
		Response response = service.path("api").path("funcionarios").request().post(Entity.entity(dto, MediaType.APPLICATION_JSON), Response.class);

		assert response.getStatus() == 400;
	}
	@Test
	public void testGetFuncionario() {
		Response response = service.path("api").path("funcionarios").request().accept(MediaType.APPLICATION_JSON).get();
		assert response.getStatus() == 200;
		assertNotNull(response.getEntity());
	}

	@Test
	public void testGetOneFuncionario() {
		Response response = service.path("api").path("funcionarios").path("12").request().accept(MediaType.APPLICATION_JSON).get();
		assert response.getStatus() == 200;
		assertNotNull(response.getEntity());
	}
	@Test
	public void testGetOneFuncionario404() {
		Response response = service.path("api").path("funcionarios").path("300").request().accept(MediaType.APPLICATION_JSON).get();
		assert response.getStatus() == 404;
		assertNotNull(response.getEntity());
	}
	@Test 
	public void testPutFuncionario() {
		FuncionarioDTO dto = new FuncionarioDTO();
		dto.setId(12);
		dto.setLotacao(1);
		dto.setNome("Camila testesss");
		dto.setTelefone("1020-3040");
		Response response = service.path("api").path("funcionarios").path("11").request().accept(MediaType.APPLICATION_JSON).put(Entity.entity(dto, MediaType.APPLICATION_JSON));
		assert response.getStatus() == 200;
	}
	@Test 
	public void testPutFuncionario400() {
		FuncionarioDTO dto = new FuncionarioDTO();
		dto.setId(11);
		dto.setLotacao(1);
		dto.setNome("Camila testesss");
		dto.setTelefone("55555-101010");
		Response response = service.path("api").path("funcionarios").path("11").request().accept(MediaType.APPLICATION_JSON).put(Entity.entity(dto, MediaType.APPLICATION_JSON));
		assert response.getStatus() == 400;
	}

	@Test
	public void testDeleteFuncionario() {
		Response response = service.path("api").path("funcionarios").path("11").request().accept(MediaType.APPLICATION_JSON).delete();
		assert response.getStatus() == 200;
	}

//	Não soube como fazer validação quando não encontra no banco um dado pelo delete
//	@Test
//	public void testDeleteFuncionario500() {
//		Response response = service.path("api").path("funcionarios").path("500").request().accept(MediaType.APPLICATION_JSON).delete();
//		assert response.getStatus() == 200;
//	}

}

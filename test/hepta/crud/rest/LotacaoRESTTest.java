package hepta.crud.rest;

import static org.junit.Assert.assertNotNull;

import java.net.URI;
import java.util.Random;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;
import org.junit.Before;
import org.junit.Test;

import hepta.crud.dto.FuncionarioDTO;
import hepta.crud.dto.LotacaoDTO;

public class LotacaoRESTTest {

	WebTarget service;

	@Before
	public void setUp() throws Exception {
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		service = client.target(getBaseURI());
	}

	private static URI getBaseURI() {
		return UriBuilder.fromUri("http://localhost:8080/crud-jersey").build();
	}

	@Test
	public void testPostLotacao() {
		LotacaoDTO dto = new LotacaoDTO();
		Random rand = new Random();
		dto.setNome(String.format("Teste de post %d", rand.nextInt(99)));
		dto.setRamal(String.format("%d%d%d%d-%d%d%d%d", rand.nextInt(9), rand.nextInt(9), rand.nextInt(9), rand.nextInt(9), rand.nextInt(9), rand.nextInt(9), rand.nextInt(9), rand.nextInt(9)));
		Response response = service.path("api").path("lotacao").request().post(Entity.entity(dto, MediaType.APPLICATION_JSON), Response.class);

		assert response.getStatus() == 201;
	}
	
	@Test
	public void testPostLotacao400() {
		LotacaoDTO dto = new LotacaoDTO();
		Random rand = new Random();
		dto.setNome("asdfasdfffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff");
		dto.setRamal(String.format("%d%d%d%d-%d%d%d%d", rand.nextInt(9), rand.nextInt(9), rand.nextInt(9), rand.nextInt(9), rand.nextInt(9), rand.nextInt(9), rand.nextInt(9), rand.nextInt(9)));
		Response response = service.path("api").path("lotacao").request().post(Entity.entity(dto, MediaType.APPLICATION_JSON), Response.class);

		assert response.getStatus() == 400;
	}

	@Test
	public void testGetLotacao() {
		Response response = service.path("api").path("lotacao").request().accept(MediaType.APPLICATION_JSON).get();
		assert response.getStatus() == 200;
		assertNotNull(response.getEntity());
	}

	@Test
	public void testGetOneLotacao() {
		Response response = service.path("api").path("lotacao").path("1").request().accept(MediaType.APPLICATION_JSON).get();
		assert response.getStatus() == 200;
		assertNotNull(response.getEntity());
	}
	@Test
	public void testGetOneLotacao404() {
		Response response = service.path("api").path("lotacao").path("300").request().accept(MediaType.APPLICATION_JSON).get();
		assert response.getStatus() == 404;
	}
	@Test 
	public void testPutLotacao() {
		LotacaoDTO dto = new LotacaoDTO();
		dto.setId(11);
		dto.setNome("Lotacao testesss");
		dto.setRamal("1020-3040");
		Response response = service.path("api").path("lotacao").path("1").request().accept(MediaType.APPLICATION_JSON).put(Entity.entity(dto, MediaType.APPLICATION_JSON));
		assert response.getStatus() == 200;
	}
	@Test 
	public void testPutLotacao400() {
		LotacaoDTO dto = new LotacaoDTO();
		dto.setId(11);
		dto.setNome("Lotacao testesss");
		dto.setRamal("55555-101010");
		Response response = service.path("api").path("lotacao").path("11").request().accept(MediaType.APPLICATION_JSON).put(Entity.entity(dto, MediaType.APPLICATION_JSON));
		assert response.getStatus() == 400;
	}

	@Test
	public void testDeleteLotacao() {
		Response response = service.path("api").path("lotacao").path("4").request().accept(MediaType.APPLICATION_JSON).delete();
		assert response.getStatus() == 200;
	}
}
